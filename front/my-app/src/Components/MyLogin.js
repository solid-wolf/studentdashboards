import React, { Component } from 'react';
import axios from 'axios';

export class MyLogin extends Component {
    constructor(props) {
        super(props);
        this.state = { 
                         mail: " ",
                         password:" ",
                        };
        this.handleMailChange = this.handleMailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleMailChange(event) {
                this.setState({ mail: event.target.value});
        }  
    handlePasswordChange(event) {
        this.setState({
            password: event.target.value  });
    }  
  
    handleSubmit(event) {
        event.preventDefault();
        const dataLog= {
            mail: this.state.mail,
            password: this.state.password};
    
        axios.post(`http://localhost:3300/login`, dataLog)
            .then(res => {
                console.log(res);
                console.log(res.data);
            })
};
    render() {
        return (
            <div className='MyLogin' id="mylogin">
                <form onSubmit={this.handleSubmit}>
                    <h3>Login</h3>
                        <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input mail={this.state.value} onChange={this.handleMailChange}
                         name='mail' type="mail" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"/>
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                        <input password={this.state.value} onChange={this.handlePasswordChange} 
                        name='password' type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"/>
                        </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        )
    }
}

export default MyLogin
