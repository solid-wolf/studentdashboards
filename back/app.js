const express = require('express');
const cors = require('cors'); //connect requests
const bodyParser = require('body-parser');

const session = require('express-session');

//connexion data base

const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27018/ma_db', { useNewUrlParser: true }, (err, res) =>{
    if (err) {
        s
        console.log(err)
    } else {console.log('database online')}
});


//model creation

mongoose.set('useCreateIndex', true);
const userSchema = new mongoose.Schema ({
    nom: {
        type: String,
    },
    prenom: {
        type: String,
    },
    mail: {
        type: String,
        unique: true,
        require: true,
        trim : true,
    },
    password: {
        type: String,
        require: true,
    },
    marks : {
        type: Array,
       
    },
    absence: {
        type: Array,
    },
    delay: {
        type: Array,
    },
});

let User = mongoose.model('Users', userSchema);

// server parameter

const hostname = 'localhost';

const port = '3300';

const app = express();

app.use(cors());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({extended:true}));

// creation of router

let myRouter = express.Router();

app.use(myRouter);

myRouter.route('/ping')
    .get((req, res) => {
        res.send('pong')
    });

//user route

myRouter.route('/users') //inscription user
    .post((req, res) => {
        let user = new User();
        user.nom = req.body.nom;
        user.prenom = req.body.prenom;
        user.mail = req.body.mail;
        user.password = req.body.password;
        user.save((err) => {
            if(err){
                console.log(err)
            } else {
                console.log('good job user post with success')
                console.log(user)
                res.send('utilisateur ajouté avec succés')}
        });
});

myRouter.route('/login') //login user
    .post((req, res) => {
        let userData = req.body
        console.log(userData);
        User.findOne({ mail: userData.mail }, (err, userLog) => {
            if (err) {
                    console.log(err)
                } else {
                    if(!userLog){
                        res.status(401).send('bad email')       
                    }
                    else if (userLog.password !== userData.password)
                        res.status(401).send('bad password')
                    else {
                        console.log('you are registred')
                        console.log(userLog)
                        res.status(200).json(userLog)
                }
            }
         })
        
    });


myRouter.route('/admin/:id')
   .delete((req, res) => {
       console.log('Im here')
        User.deleteOne({ _id: req.params.id}, (err, users) => {
            if (err) {
                console.log(err)
                res.send(err)
            } else {
                console.log('user delete')
                console.log(users)
                res.send('utilisateur supprimé avec succés')
            }
        })
 })   
 
    .put((req, res) => {
        User.findById({_id:req.params.id}, (err, user) => {
            // console.log(req.params.id);
            // console.log(user);
            // res.end()
            if (err) {
                console.log(err)
                res.send(err)
            } else {
                // let user = new User();
                user.nom = req.body.nom;
                user.prenom = req.body.prenom;
                user.mail = req.body.mail;
                user.password = req.body.password;
                user.save((err) => {
                    if (err) {
                        console.log(err)
                    } else {
                        console.log('good job user modifie with success')
                        console.log(user)
                        res.send('utilisateur modifié avec succés')
                    }
                });
            }
        })
})

    .get((req, res) => {
        User.findById({ _id: req.params.id }, { nom: 1, prenom: 1, mail:1, _id: 0 }, (err, user) => {
            if (err) {
                console.log(err)
                res.send(err)
            } else {
                console.log(user)
                res.json(user)
            }
        });
    })

myRouter.route('/admin/marks/:id')
    .put((req, res) => {
        User.findById({ _id: req.params.id }, (err, user) => {
            // console.log(req.params.id);
            // console.log(user);
            // res.end()
            if (err) {
                console.log(err)
                res.send(err)
            } else {
                // let user = new User();
                
                user.marks = req.body.marks;
                user.save((err) => {
                    if (err) {
                        console.log(err)
                    } else {
                        console.log('good job marks modifie with success')
                        console.log(user)
                        res.send('notes modifié avec succés')
                    }
                });
            }
   })
})
    .get((req, res) => {
        User.findById({ _id: req.params.id },{nom:1, marks:1, _id:0}, (err, user) => {
            if (err) {
                console.log(err)
                res.send(err)
            } else {
                console.log(user)
                res.json(user)
            }
        });
})
//starting the server

app.listen(port, hostname, () =>{
    console.log('mon serveur fonctionne sur http://' + hostname +':' + port + '\n');
});